package day4;

import utils.InputReader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.OptionalInt;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ReposeRecord {

    public static void main(String[] args) {
        List<String> guardRecords = readGuardRecords();
        Collections.sort(guardRecords);
        List<GuardObservation> guardObservations = createGuardObservations(guardRecords);
        List<Guard> guards = sumGuardObservations(guardObservations);


        Guard guardSleptTheMost = guards.get(0);
        for (int i = 1; i < guards.size(); i++) {
            if (guards.get(i).getSumMinutesTheGuardSlept() > guardSleptTheMost.getSumMinutesTheGuardSlept()) {
                guardSleptTheMost = guards.get(i);
            }
        }

        System.out.println(guardSleptTheMost);
        System.out.println(guardSleptTheMost.getSumMinutesTheGuardSlept());
        System.out.println(guardSleptTheMost.getMinuteTheGuardSleepsTheMost());


    }

    private static List<Guard> sumGuardObservations(List<GuardObservation> guardObservations) {

        List<Guard> guards = new ArrayList<>();

        for (Integer guardId : distinctGuardIds(guardObservations)) {
            int[] asleepPerMinute = new int[60];
            for (GuardObservation observation : allObservationsForAGuard(guardObservations, guardId)) {
                for (int i = 0; i < 60; i++) {
                    asleepPerMinute[i] += observation.getAsleepPerMinute()[i];
                }
            }
            guards.add(new Guard(guardId, asleepPerMinute));
        }

        return guards;
    }

    private static List<GuardObservation> allObservationsForAGuard(List<GuardObservation> guardObservations, Integer guardId) {
        return guardObservations.stream()
                    .filter(o -> o.getId() == guardId)
                    .collect(Collectors.toList());
    }

    private static List<Integer> distinctGuardIds(List<GuardObservation> guardObservations) {
        return guardObservations.stream()
                .map(GuardObservation::getId)
                .distinct()
                .collect(Collectors.toList());
    }

    private static List<GuardObservation> createGuardObservations(List<String> guardRecords) {
        Pattern idPattern = Pattern.compile("#(\\d*)");
        Matcher matcher;

        List<GuardObservation> guards = new ArrayList<>();

        boolean awake = true;
        int minuteFellAsleep = 0;
        GuardObservation currentGuard = null;

        for (String entry : guardRecords) {

            matcher = idPattern.matcher(entry);
            boolean isGuardBeginsShift = matcher.find();

            if (isGuardBeginsShift) {
                String guardId = matcher.group(1);

                currentGuard = new GuardObservation(Integer.parseInt(guardId));
                guards.add(currentGuard);

                //nightWatch.add(currentGuard);
                //currentGuard = nightWatch.retrieveGuard(currentGuard.getId());
            } else {
                String minute = entry.substring(15, 17);
                if (awake) {
                    minuteFellAsleep = Integer.parseInt(minute);
                    awake = false;
                } else {
                    int minuteAwoke = Integer.parseInt(minute);
                    awake = true;

                    //Record minutes asleep to current guard
                    for (int i = minuteFellAsleep; i < minuteAwoke; i++) {
                        currentGuard.getAsleepPerMinute()[i] += 1;
                    }
                }
            }
        }
        return guards;
    }

    private static List<String> readGuardRecords() {
        return InputReader.readFile("day4input");
    }
}
