package day4;

public class GuardObservation {
    private int id;
    private int[] asleepPerMinute;

    public GuardObservation(int id) {
        this.id = id;
        this.asleepPerMinute = new int[60];
    }

    public int reportMinuteMostOftenAsleep() {
        int max = 0;
        int index = 0;
        for (int i = 0; i < asleepPerMinute.length; i++) {
            if (asleepPerMinute[i] > max) {
                max = asleepPerMinute[i];
                index = i;
            }
        }
        return index;
    }

    public int reportFrequencyOfMinuteMostOftenAsleep() {
        return asleepPerMinute[reportMinuteMostOftenAsleep()];
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int[] getAsleepPerMinute() {
        return asleepPerMinute;
    }

    public void setAsleepPerMinute(int[] asleepPerMinute) {
        this.asleepPerMinute = asleepPerMinute;
    }


    @Override
    public String toString() {
        return "GuardGuardObservationrd{" +
                "id=" + id +
                ", \nasleepPerMinute=" + asleepPerMinute +
                "}\n\n";
    }
}
