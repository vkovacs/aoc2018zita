package day4;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Guard {
    private int id;
    private int[] sumSleepMinutes;

    public Guard(int id, int[] sumSleepMinutes) {
        this.id = id;
        this.sumSleepMinutes = sumSleepMinutes;
    }

    public int getId() {
        return id;
    }

    public int[] getSumSleepMinutes() {
        return sumSleepMinutes;
    }

    int getSumMinutesTheGuardSlept() {
        return Arrays.stream(sumSleepMinutes).sum();
    }

    int getMinuteTheGuardSleepsTheMost() {
        int maxIndex = 0;
        for (int i = 0; i < 60; i++) {
            if (sumSleepMinutes[i] > sumSleepMinutes[maxIndex]) {
                maxIndex = i;
            }
        }
        return maxIndex;
    }

    @Override
    public String toString() {
        return "Guard{" +
                "id=" + id +
                ", sumSleepMinutes=" + Arrays.toString(sumSleepMinutes) +
                '}';
    }
}
